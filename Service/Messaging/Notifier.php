<?php
/**
 * Created by PhpStorm.
 * User: ScorpioT1000
 * Date: 10.05.2018
 * Time: 23:02
 */

namespace CommonBundle\Service\Messaging;


use CommonBundle\Document\User;
use CommonBundle\Manager\UserNotificationManager;
use Symfony\Bundle\TwigBundle\TwigEngine;

class Notifier
{
    /** @var \CommonBundle\Manager\UserNotificationManager */
    protected $userNotificationManager;

    /** @var \Symfony\Bundle\TwigBundle\TwigEngine */
    protected $twig;

    /** @var array "initial" parameters from config */
    protected $initial;

    /**
     * @param \CommonBundle\Manager\UserNotificationManager $userNotificationManager
     * @param \Symfony\Bundle\TwigBundle\TwigEngine $twig
     * @param array $initial
     */
    public function __construct(UserNotificationManager $userNotificationManager, TwigEngine $twig, array $initial) {
        $this->userNotificationManager = $userNotificationManager;
        $this->twig = $twig;
        $this->initial = $initial;
    }

    /**
     * @param \CommonBundle\Document\User $user
     * @param string $title Can be a path to view or string, detected automatically
     * @param string $content Can be a path to view or string, detected automatically
     * @param array $viewParams
     */
    public function sendSystemMessage(
        User $user,
        string $title,
        string $content,
        array $viewParams = []
    ) {
        $params = array_merge($this->initial, $viewParams);

        if(substr($content, -5) === '.twig') {
            $content = $this->twig->render($content, $params);
        }
        if(substr($title, -5) === '.twig') {
            $title = $this->twig->render($title, $params);
        }

        $this->userNotificationManager->send($user, $title, $content);
    }
}