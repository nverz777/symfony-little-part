<?php
/**
 * Created by PhpStorm.
 * User: ScorpioT1000
 * Date: 10.05.2018
 * Time: 23:02
 */

namespace CommonBundle\Service\Messaging;


use CommonBundle\Service\Messaging\Helper\Button;
use Symfony\Bundle\TwigBundle\TwigEngine;

class Mailer
{
    /** @var \Swift_Mailer */
    protected $swift;

    /** @var \Symfony\Bundle\TwigBundle\TwigEngine */
    protected $twig;

    /** @var array "initial" parameters from config */
    protected $initial;

    /** @var string */
    protected $mailFrom;

    /**
     * @param \Swift_Mailer $swift
     * @param \Symfony\Bundle\TwigBundle\TwigEngine $twig
     * @param array $initial
     * @param string $mailFrom
     */
    public function __construct(\Swift_Mailer $swift, TwigEngine $twig, array $initial, string $mailFrom) {
        $this->swift = $swift;
        $this->twig = $twig;
        $this->initial = $initial;
        $this->mailFrom = $mailFrom;
    }

    /**
     * @param string $targetEmail
     * @param string $subject Can be a path to view or string, detected automatically
     * @param string $view Can be a path to view or string, detected automatically
     * @param array $viewParams
     * @param Button|Button[] $buttons
     * @return int The number of successful recipients
     */
    public function sendSystemMessage(
        string $targetEmail,
        string $subject,
        string $view,
        array $viewParams = [],
        $buttons = []
    ): int {
        $params = array_merge($this->initial, $viewParams);
        if(! is_array($buttons)) {
            $buttons = [$buttons];
        }
        $params['buttons'] = $buttons;

        if(substr($view, -5) === '.twig') {
            $view = $this->twig->render($view, $params);
        }
        if(substr($subject, -5) === '.twig') {
            $subject = $this->twig->render($subject, $params);
        }

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setTo($targetEmail)
            ->setFrom($this->mailFrom, $this->initial['brand'])
            ->setBody($view, 'text/html');

        return $this->swift->send($message);
    }
}