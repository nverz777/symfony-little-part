<?php
/**
 * Created by PhpStorm.
 * User: ScorpioT1000
 * Date: 11.05.2018
 * Time: 17:04
 */

namespace CommonBundle\Service\Messaging\Helper;

/**
 * Class Button immutable
 */
class Button
{
    const TYPE_PRIMARY = 'primary';
    const TYPE_SECONDARY = 'secondary';
    const TYPE_WARNING = 'warning';

    protected $text;
    protected $uri;
    protected $type;
    protected $isRelativeUri = false;

    /**
     * @param string $text
     * @param string $uri can be relative like "/login"
     * @param string $type
     */
    public function __construct(string $text, string $uri, string $type = self::TYPE_PRIMARY)
    {
        $this->text = $text;
        $this->uri = $uri;
        $this->type = $type;
        if($this->uri[0] === '/') {
            $this->isRelativeUri = true;
        }
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isRelativeUri(): bool
    {
        return $this->isRelativeUri;
    }
}