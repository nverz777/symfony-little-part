<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 11.01.2018
 * Time: 15:41
 */

namespace CommonBundle\Repository;


use CommonBundle\Document\User;
use Doctrine\ODM\MongoDB\DocumentRepository;

class AbstractRepository extends DocumentRepository
{
    /**
     * @return int
     */
    public function countAll() {
        return $this->createQueryBuilder()
            ->count()
            ->getQuery()
            ->execute();
    }

    /**
     * Can be called for the documents with the field "user" only.
     * @param \CommonBundle\Document\User $user
     * @return int
     */
    public function countAllForUser(User $user) {
        return $this->createQueryBuilder()
                    ->field('user')
                    ->equals($user)
                    ->count()
                    ->getQuery()
                    ->execute();
    }

    /**
     * Finds documents by a set of criteria. Indexes by identifier field.
     *
     * @param array        $criteria Query criteria
     * @param array        $sort     Sort array for Cursor::sort()
     * @param integer|null $limit    Limit for Cursor::limit()
     * @param integer|null $skip     Skip for Cursor::skip()
     *
     * @return array indexed by identifier
     */
    public function findByIndexed(array $criteria, array $sort = null, $limit = null, $skip = null) {
        return $this->getDocumentPersister()->loadAll($criteria, $sort, $limit, $skip)->toArray(true);
    }
}