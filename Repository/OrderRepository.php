<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 27.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\User;
use CommonBundle\Document\Order;

/**
 * @method Order|null find($id, $lockMode = 0, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria)
 * @method Order[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method Order[] findAll()
 */
class OrderRepository extends AbstractRepository
{
    /**
     * @param \CommonBundle\Document\Order $order
     * @return \CommonBundle\Document\Order[]
     */
    public function findJointsFor(Order $order)
    {
        $qb = $this->createQueryBuilder();

        if(empty($order->getJointOrderIds())) {
            return [];
        }
        return $qb->field('id')->in($order->getJointOrderIds())->getQuery()->execute()->toArray();
    }
}