<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 09.01.2018
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\Lot;

/**
 * @method Lot|null find($id, $lockMode = 0, $lockVersion = null)
 * @method Lot|null findOneBy(array $criteria)
 * @method Lot[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method Lot[] findAll()
 */
class LotRepository extends AbstractRepository
{
}