<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\TradeUnit;
use Symfony\Component\Validator\Constraints\DateTime;
use TransformationsBundle\Utilities\DateTimeConverter;

/**
 * @method TradeUnit|null find($id, $lockMode = 0, $lockVersion = null)
 * @method TradeUnit|null findOneBy(array $criteria)
 * @method TradeUnit[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method TradeUnit[] findAll()
 */
class TradeUnitRepository extends AbstractRepository
{
    protected $platform = null;

    public function usePlatform(string $platform): TradeUnitRepository
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @return TradeUnit|null
     */
    public function findLastTrade()
    {
        $result = $this->findBy(['platform' => $this->platform], ['at' => -1], 1);
        if(empty($result)) {
            return null;
        }
        return reset($result);
    }

    /**
     * Tries to find latest units since the specified date.
     * If the date is not specified or nothing found, retrieves latest records, no more than $altLimit
     * @param string $type
     * @param int $limit
     * @return \CommonBundle\Document\TradeUnit[]|null
     * @see \CommonBundle\Document\Types\CurrencyPair[] ordered by date ascending
     */
    public function findLatestLimited(string $type, int $limit) {
        return array_reverse($this->findBy(['platform' => $this->platform, 'type' => $type], ['at' => -1], $limit));
    }

    /**
     * @param string|null $type
     * @param \DateTime $since
     * @param \DateTime|null $until
     * @param bool $sinceInclusive if true, uses ">=" otherwise uses ">" for $since
     * @return \CommonBundle\Document\TradeUnit[]
     */
    public function findLatest(
        string $type = null,
        \DateTime $since,
        \DateTime $until = null,
        bool $sinceInclusive = true
    ) {
        $qb = $this->createQueryBuilder();

        if ($sinceInclusive) {
            $qb->field('at')->gte($since);
        } else {
            $qb->field('at')->gt($since);
        }

        if($until) {
            $qb->lte($until);
        }
        if($type) {
            $qb->field('type')->equals($type);
        }

        $qb->field('platform')->equals($this->platform);

        $qb->sort('at', 1);

        return $qb->getQuery()->execute()->toArray();
    }

    /**
     * @param string $type
     * @return \CommonBundle\Document\TradeUnit|null
     */
    public function findOneLatest(string $type) {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('type')->equals($type)
            ->field('platform')->equals($this->platform)
            ->limit(1)
            ->sort('at', -1);

        $result = $qb->getQuery()->execute()->toArray();
        return $result ? reset($result) : null;
    }

    /**
     * Fast method, returns units in raw database format
     * @param string|null $type
     * @param \DateTime $since
     * @param \DateTime|null $until
     * @param bool $sinceInclusive if true, uses ">=" otherwise uses ">" for $since
     * @return \Doctrine\ODM\MongoDB\Cursor
     */
    public function findLatestRaw(
        string $type = null,
        \DateTime $since,
        \DateTime $until = null,
        bool $sinceInclusive = true
    ) {
        $qb = $this->createQueryBuilder();

        if ($sinceInclusive) {
            $qb->field('at')->gte($since);
        } else {
            $qb->field('at')->gt($since);
        }

        if($until) {
            $qb->lte($until);
        }
        if($type) {
            $qb->field('type')->equals($type);
        }

        $qb->field('platform')->equals($this->platform);

        $qb->sort('at', 1);
        $qb->hydrate(false);

        return $qb->getQuery()->execute();
    }

    /**
     * Finds for each platforms
     * @param string|null $type
     * @param int $forLastDays from the specified days before
     * @param \DateTime|null $until
     * @return \CommonBundle\Document\TradeUnit[]
     */
    public function findLatestAggregatedByDays(string $type = null, int $forLastDays, \DateTime $until = null)
    {
        $now = $until ?: new \DateTime();
        $since = (clone $now)->sub(new \DateInterval('P'.$forLastDays.'D'));
        $qb = $this->createAggregationBuilder();

        $match = $qb
            ->hydrate(TradeUnit::class)
            ->match()
                ->field('at')
                ->gt($since);
        if($until) {
            $match->lte($until);
        }
        if($type) {
            $match->field('type')->equals($type);
        }

        if($type) {
            $idExpr = $qb->expr()->concat(
                '$platform',
                $qb->expr()->dateToString('%Y-%m-%d', '$at')
            );
        } else {
            $idExpr = $qb->expr()->concat(
                '$platform',
                '$type',
                $qb->expr()->dateToString('%Y-%m-%d', '$at')
            );
        }

        return $match->group()
                ->field('id')
                ->expression($idExpr)
                ->field('at')
                ->first('$at')
                ->field('type')
                ->first('$type')
                ->field('price')
                ->avg('$price')
                ->field('priceMin')
                ->min($qb->expr()->ifNull('$priceMin', '$price'))
                ->field('priceMax')
                ->max($qb->expr()->ifNull('$priceMax', '$price'))
                ->field('priceOpen')
                ->first($qb->expr()->ifNull('$priceOpen', '$price'))
                ->field('priceClose')
                ->last($qb->expr()->ifNull('$priceClose', '$price'))
                ->field('count')
                ->sum($qb->expr()->ifNull('$count', 1))
            ->sort('at', 1)
            ->execute()
            ->toArray();
    }

    /**
     * Finds for each platforms
     * @param \DateTime $dt
     * @return TradeUnit[]|\Doctrine\MongoDB\CursorInterface
     */
    public function findAllBefore(\DateTime $dt) {
        return $this
            ->createQueryBuilder()
            ->field('at')
            ->lt($dt)
            ->getQuery()
            ->execute();
    }
}