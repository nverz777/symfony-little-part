<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\User;
use CommonBundle\Document\UserAuthDetail;

/**
 * @method UserAuthDetail|null find($id, $lockMode = 0, $lockVersion = null)
 * @method UserAuthDetail|null findOneBy(array $criteria)
 * @method UserAuthDetail[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method UserAuthDetail[] findAll()
 */
class UserAuthDetailRepository extends AbstractRepository
{
    /**
     * @param \CommonBundle\Document\User $user
     * @param \DateTime|null $since
     * @param \DateTime|null $until
     * @param int $count Count of details to load if since is null
     * @return \CommonBundle\Document\UserAuthDetail[]
     */
    public function findLatest(
        User $user,
        \DateTime $since = null,
        \DateTime $until = null,
        int $count = 10
    ) {
        $qb = $this->createQueryBuilder();

        $qb->field('user')->references($user);

        if ($since) {
            $qb->field('createdAt')->gt($since);
        } else {
            $qb->limit($count);
        }
        if ($until) {
            $qb->field('createdAt')->lte($until);
        }

        $qb->sort('createdAt', -1);

        return $qb->getQuery()->execute()->toArray();
    }
}