<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\SimulatorAccount;
use CommonBundle\Service\TradingPlatform\Provider\PlatformLinkInterface;

/**
 * @method SimulatorAccount|null find($id, $lockMode = 0, $lockVersion = null)
 * @method SimulatorAccount|null findOneBy(array $criteria)
 * @method SimulatorAccount[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method SimulatorAccount[] findAll()
 */
class SimulatorAccountRepository extends AbstractRepository
{

    /**
     * @param string[] $ids
     * @return SimulatorAccount[] indexed by currency
     */
    public function findByIdsIndexedByCurrency(array $ids)
    {
        /** @var SimulatorAccount[] $found */
        $found = $this->createQueryBuilder()
            ->field('id')
            ->in($ids)
            ->getQuery()
            ->execute();
        $results = [];
        foreach($found as $acc) {
            $results[$acc->getCurrency()] = $acc;
        }
        return $results;
    }

    /**
     * @param \CommonBundle\Service\TradingPlatform\Provider\PlatformLinkInterface $pl
     * @return \CommonBundle\Document\SimulatorAccount[]
     */
    public function findByPlatformLink(PlatformLinkInterface $pl)
    {
        return $this->findByIdsIndexedByCurrency(
            [$pl->getBaseAccountId(), $pl->getQuoteAccountId()]
        );
    }
}