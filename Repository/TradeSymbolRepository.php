<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\TradeSymbol;
use CommonBundle\Document\APIPass;
use CommonBundle\Service\TradingPlatform\Manager\TradingPlatformManager;

/**
 * @method TradeSymbol|null find($id, $lockMode = 0, $lockVersion = null)
 * @method TradeSymbol|null findOneBy(array $criteria)
 * @method TradeSymbol[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method TradeSymbol[] findAll()
 */
class TradeSymbolRepository extends AbstractRepository
{
    /**
     * @param \CommonBundle\Service\TradingPlatform\TradingPlatformInterface|string $tradingPlatform
     * @return TradeSymbol[]
     */
    public function findAllByTradingPlatform($tradingPlatform)
    {
        $tradingPlatform = TradingPlatformManager::classOf($tradingPlatform);

        return $this->findBy(['tradingPlatform' => $tradingPlatform]);
    }

    /**
     * @param \CommonBundle\Service\TradingPlatform\TradingPlatformInterface|string $tradingPlatform
     * @param string $type
     * @return \CommonBundle\Document\TradeSymbol
     */
    public function findOneByTradingPlatformAndType($tradingPlatform, string $type)
    {
        $tradingPlatform = TradingPlatformManager::classOf($tradingPlatform);

        return $this->findOneBy(['tradingPlatform' => $tradingPlatform, 'type' => $type]);
    }
}