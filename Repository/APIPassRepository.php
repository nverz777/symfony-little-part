<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\APIPass;

/**
 * @method APIPass|null find($id, $lockMode = 0, $lockVersion = null)
 * @method APIPass|null findOneBy(array $criteria)
 * @method APIPass[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method APIPass[] findAll()
 */
class APIPassRepository extends AbstractRepository
{
}