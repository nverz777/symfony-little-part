<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 09.01.2018
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\User;
use CommonBundle\Document\ArchiveTrade;

/**
 * @method ArchiveTrade|null find($id, $lockMode = 0, $lockVersion = null)
 * @method ArchiveTrade|null findOneBy(array $criteria)
 * @method ArchiveTrade[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method ArchiveTrade[] findAll()
 */
class ArchiveTradeRepository extends AbstractRepository
{
    /**
     * @param \CommonBundle\Document\User $user
     * @return \CommonBundle\Document\ArchiveTrade[]
     */
    public function findAllForUser(User $user)
    {
        return $this->findBy(['user' => $user]);
    }
}