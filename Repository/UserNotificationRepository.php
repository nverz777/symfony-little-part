<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\User;
use CommonBundle\Document\UserNotification;

/**
 * @method UserNotification|null find($id, $lockMode = 0, $lockVersion = null)
 * @method UserNotification|null findOneBy(array $criteria)
 * @method UserNotification[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method UserNotification[] findAll()
 */
class UserNotificationRepository extends AbstractRepository
{
    /**
     * @param \CommonBundle\Document\User $user
     * @param string[] $statuses Specify to filter by statuses
     * @return UserNotification[] sorted by date, new first
     */
    public function findAllFor(User $user, array $statuses = []) {
        $qb = $this->createQueryBuilder()
            ->field('user')->references($user);

        if($statuses) {
            $qb->field('status')->in($statuses);
        }

        $qb->sort('createdAt', -1);

        return $qb->getQuery()->execute()->toArray();
    }

    /**
     * @param \CommonBundle\Document\User $user
     * @param array $ids
     * @return UserNotification[] sorted by date, new first
     */
    public function findByIdsFor(User $user, array $ids) {
        return $this->createQueryBuilder()
            ->field('user')->references($user)
            ->field('id')->in($ids)
            ->getQuery()->execute()->toArray();
    }
}