<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 10.12.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\PaymentToken;

/**
 * @method PaymentToken|null find($id, $lockMode = 0, $lockVersion = null)
 * @method PaymentToken|null findOneBy(array $criteria)
 * @method PaymentToken[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method PaymentToken[] findAll()
 */
class PaymentTokenRepository extends AbstractRepository
{
    /**
     * @param \DateTime $dt
     * @return PaymentToken[]|\Doctrine\MongoDB\CursorInterface
     */
    public function findAllEarlierThan(\DateTime $dt) {
        return $this
            ->createQueryBuilder()
            ->field('createdAt')
            ->lt($dt)
            ->getQuery()
            ->execute();
    }
}