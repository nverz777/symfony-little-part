<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 18.10.2017
 * Time: 15:24
 */

namespace CommonBundle\Repository;

use CommonBundle\Document\SystemSetting;
use MongoDB\BSON\Regex;

/**
 * @method SystemSetting|null find($id, $lockMode = 0, $lockVersion = null)
 * @method SystemSetting|null findOneBy(array $criteria)
 * @method SystemSetting[] findBy(array $criteria, array $sort = null, $limit = null, $skip = null)
 * @method SystemSetting[] findAll()
 */
class SystemSettingRepository extends AbstractRepository
{
    const FINDBYKEYPART_LEFT =  1;
    const FINDBYKEYPART_RIGHT = 2;
    const FINDBYKEYPART_BOTH =  4;

    /**
     * @param string $keyPart
     * @param int $type
     * @param bool $indexByKey
     * @return \CommonBundle\Document\SystemSetting[]
     */
    public function findByKeyPart(string $keyPart, $type = self::FINDBYKEYPART_LEFT, bool $indexByKey = false) {
        $pattern = $keyPart;
        if($type === static::FINDBYKEYPART_LEFT) {
            $pattern = '^'.$pattern;
        } else if($type === static::FINDBYKEYPART_RIGHT) {
            $pattern .= '$';
        }
        $result = $this->findBy(['key' => ['$regex' => new Regex($pattern, '')]], ['key' => 1]);
        if($indexByKey) {
            $resultIndexed = [];
            foreach($result as $setting) {
                $resultIndexed[$setting->getKey()] = $setting;
            }
            return $resultIndexed;
        }
        return $result;
    }

    /**
     * @param string $key
     * @return \CommonBundle\Document\SystemSetting|null
     */
    public function findOneByKey(string $key) {
        return $this->findOneBy(['key' => $key]);
    }
}