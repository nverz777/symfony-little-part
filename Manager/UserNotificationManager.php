<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 09.01.2018
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\Types\NotificationStatus;
use CommonBundle\Document\User;
use CommonBundle\Document\UserNotification;
use CommonBundle\Repository\UserNotificationRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method UserNotificationRepository getRepository()
 */
class UserNotificationManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, UserNotification::class);
    }

    /**
     * @param \CommonBundle\Document\User $user
     * @param string $title
     * @param string $content
     * @param string|null $type
     * @param bool $andFlush
     */
    public function send(
        User $user,
        string $title,
        string $content,
        string $type = null,
        bool $andFlush = true
    ) {
        $notification = new UserNotification($user, $title, $content, $type ?: UserNotification::TYPE_SYSTEM);
        $this->persist($notification);
        if($andFlush) {
            $this->flush();
        }
    }

    /**
     * @param \CommonBundle\Document\User $user
     * @param array $ids
     * @param bool $andFlush
     * @return int actually marked
     */
    public function markAsRead(User $user, array $ids, bool $andFlush = true)
    {
        $found = $this->getRepository()->findByIdsFor($user, $ids);
        $count = 0;

        foreach($found as $notification) {
            $notification->setStatus(NotificationStatus::READ);
            $this->persist($notification);
            ++$count;
        }

        if($andFlush) {
            $this->flush();
        }

        return $count;
    }

    /**
     * @param \CommonBundle\Document\User $user
     */
    public function removeAllFor(User $user)
    {
        $found = $this->getRepository()->findAllFor($user);
        foreach($found as $n) {
            $this->remove($n);
        }
        $this->flush();
    }
}