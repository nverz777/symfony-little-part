<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 14.12.2017
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\Order;
use CommonBundle\Repository\OrderRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method OrderRepository getRepository()
 */
class OrderManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, Order::class);
    }

}