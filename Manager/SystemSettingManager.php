<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 05.10.2017
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\SystemSetting;
use CommonBundle\Repository\SystemSettingRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * Class SystemSettingManager
 * @package CommonBundle\Manager
 *
 * @method SystemSettingRepository getRepository()
 */
class SystemSettingManager extends AbstractManager
{
    /**
     * SystemSettingManager constructor.
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, SystemSetting::class);
    }

    /**
     * Atomically creates or updates a setting with the specified value
     * @param string $key
     * @param array|int|double|bool|string|null $value
     * @return SystemSetting
     */
    public function set(string $key, $value)
    {
        $qb = $this->getRepository()->createQueryBuilder();

        /** @var SystemSetting $result */
        $result = $qb->findAndUpdate()
            ->returnNew(true)
            ->upsert(true)
            ->field('key')->equals($key)
            ->field('key')->set($key, true)
            ->field('value')->set(SystemSetting::transformValueToSave($value), true)
            ->getQuery()->execute();

        if(! $result->hasCreatedAt()) {
            $result->touchCreatedAt()->touchUpdatedAt();
            $this->persist($result)->flush();
        }

        return $result;
    }
}