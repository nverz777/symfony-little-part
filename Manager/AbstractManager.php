<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 05.10.2017
 * Time: 13:54
 */

namespace CommonBundle\Manager;

use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * Class AbstractManager
 * @package CommonBundle\Manager
 */
abstract class AbstractManager
{
    protected $documentClass;

    protected $dm;

    /**
     * AbstractManager constructor.
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     * @param string $documentClass
     */
    public function __construct(DocumentManager $documentManager, string $documentClass) {
        $this->dm = $documentManager;
        $this->documentClass = $documentClass;
    }


    /**
     * Gets the repository for current document class.
     * @return \Doctrine\ODM\MongoDB\DocumentRepository
     */
    public function getRepository()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->dm->getRepository($this->documentClass);
    }

    /**
     * @param $document
     * @return static
     */
    public function persist($document)
    {
        $this->dm->persist($document);
        return $this;
    }

    /**
     * @param $document
     * @return static
     */
    public function refresh($document)
    {
        $this->dm->refresh($document);
        return $this;
    }

    /**
     * @param $document
     * @return static
     */
    public function remove($document)
    {
        $this->dm->remove($document);
        return $this;
    }

    /**
     * Sends changes of all documents of all types to the database
     * @return static
     */
    public function flush()
    {
        $this->dm->flush();
        return $this;
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getDocumentManager()
    {
        return $this->dm;
    }
}