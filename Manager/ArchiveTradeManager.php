<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 09.01.2018
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\ArchiveTrade;
use CommonBundle\Repository\ArchiveTradeRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method ArchiveTradeRepository getRepository()
 */
class ArchiveTradeManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, ArchiveTrade::class);
    }
}