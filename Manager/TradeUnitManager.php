<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 14.12.2017
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\TradeUnit;
use CommonBundle\Document\User;
use CommonBundle\Service\TradingPlatform\Helper\TradingPlatformContainer;
use CommonBundle\Service\TradingPlatform\Manager\TradingPlatformManager;
use Doctrine\ODM\MongoDB\DocumentManager;

class TradeUnitManager extends AbstractManager
{
    /** @var \CommonBundle\Service\TradingPlatform\Helper\TradingPlatformContainer */
    protected $tradingPlatformContainer;

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     * @param \CommonBundle\Service\TradingPlatform\Helper\TradingPlatformContainer $tradingPlatformContainer
     */
    public function __construct(
        DocumentManager $documentManager,
        TradingPlatformContainer $tradingPlatformContainer
    ) {
        parent::__construct($documentManager, TradeUnit::class);

        $this->tradingPlatformContainer = $tradingPlatformContainer;
    }

    /**
     * Gets the repository for current document class. Uses default platform.
     * @return \CommonBundle\Repository\TradeUnitRepository
     */
    public function getRepository()
    {
        /** @var \CommonBundle\Repository\TradeUnitRepository $repository */
        $repository = parent::getRepository();

        return $repository->usePlatform($this->tradingPlatformContainer->getDefaultClass());
    }

    /**
     * Gets the repository for current document class and trading platform.
     * If ::requestTradesSupported() returns false for the platform, uses default.
     * @see CommonBundle/Resources/config/config.yml
     * @param string $platformClass Which platform to use.
     * @return \CommonBundle\Repository\TradeUnitRepository
     */
    public function getRepositoryFor(string $platformClass)
    {
        /** @var \CommonBundle\Repository\TradeUnitRepository $repository */
        $repository = parent::getRepository();

        $platformClass = TradingPlatformManager::classOf($platformClass);

        $platform = $this->tradingPlatformContainer->getPlatform($platformClass);

        if($platform::features()->requestTrades || $platform::features()->requestMultipleTrades) {
            return $repository->usePlatform($platformClass);
        }

        return $this->getRepository();
    }

    /**
     * @param \DateInterval $di from now
     */
    public function removeAllTradeUnitsOlderThan(\DateInterval $di) {
        $dt = (new \DateTime())->sub($di);
        $qb = $this->getRepository()->createQueryBuilder();

        $qb->remove()
            ->field('at')->lt($dt)
            ->getQuery()->execute();
    }

    /**
     * @param \DateInterval $di from now
     * @return int count removed
     */
    public function groupByDayAllTradeUnitsOlderThan(\DateInterval $di) {
        $dt = (new \DateTime())->sub($di);
        
        $allUnits = $this->getRepository()->findAllBefore($dt);
        $aggregatedUnits = $this->getRepository()->findLatestAggregatedByDays(null, 30, $dt);

        $relevantUnits = [];
        foreach($aggregatedUnits as $unit) {
            $relevantUnits[$unit->getContentIndex()] = $unit;
        }

        $removed = 0;

        foreach($allUnits as $unit) {
            if(! array_key_exists($unit->getContentIndex(), $relevantUnits)) {
                ++$removed;
                $this->remove($unit);
            }
        }

        $this->flush();

        return $removed;
    }
}