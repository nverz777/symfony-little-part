<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 09.01.2018
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\Lot;
use CommonBundle\Repository\LotRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method LotRepository getRepository()
 */
class LotManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, Lot::class);
    }

}