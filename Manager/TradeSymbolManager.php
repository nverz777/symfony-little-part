<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 14.12.2017
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\TradeSymbol;
use CommonBundle\Repository\TradeSymbolRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method TradeSymbolRepository getRepository()
 */
class TradeSymbolManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, TradeSymbol::class);
    }

    /**
     * Creates, updates or deletes all TradeSymbol in the db according to the specified values for the platform
     * @param \CommonBundle\Service\TradingPlatform\TradingPlatformInterface|string $tradingPlatform
     * @param TradeSymbol[] $tradeSymbols indexed by CurrencyPair
     * @param bool $andFlush call flush() after the operation
     * @return array With "created", "updated" and "deleted" keys and values as counts
     * @see \CommonBundle\Document\Types\CurrencyPair
     */
    public function refreshAllForPlatform($tradingPlatform, array $tradeSymbols, bool $andFlush = true)
    {
        $existingResults = $this->getRepository()->findAllByTradingPlatform($tradingPlatform);
        $result = ['created' => 0, 'updated' => 0, 'deleted' => 0];

        foreach($existingResults as $existing) {
            if(isset($tradeSymbols[$existing->getType()])) {
                $existing->updateFrom($tradeSymbols[$existing->getType()]);
                $tradeSymbols[$existing->getType()] = $existing;
                $this->persist($existing);
                $result['updated']++;
            } else {
                $this->remove($existing);
                $result['deleted']++;
            }
        }

        foreach($tradeSymbols as $unit) {
            if(! $unit->getId()) {
                $result['created']++;
            }
            $this->persist($unit);
        }

        if($andFlush) {
            $this->flush();
        }

        return $result;
    }

    /**
     * @return int removed count
     */
    public function removeAll()
    {
        $symbols = $this->getRepository()->findAll();
        $i = 0;

        foreach($symbols as $symbol) {
            $this->remove($symbol);
            ++$i;
        }

        $this->flush();
        return $i;
    }
}