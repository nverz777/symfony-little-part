<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 09.01.2018
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use CommonBundle\Document\UserAuthDetail;
use CommonBundle\Repository\UserAuthDetailRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method UserAuthDetailRepository getRepository()
 */
class UserAuthDetailManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, UserAuthDetail::class);
    }

}