<?php
/**
 * Created by PhpStorm.
 * User: Scorp
 * Date: 14.12.2017
 * Time: 13:53
 */

namespace CommonBundle\Manager;


use Brick\Math\BigDecimal;
use CommonBundle\Document\BotSettings;
use CommonBundle\Document\PlatformLink;
use CommonBundle\Document\SimulatorAccount;
use CommonBundle\Document\APIPass;
use CommonBundle\Document\Types\CurrencyPair;
use CommonBundle\Document\User;
use CommonBundle\Repository\SimulatorAccountRepository;
use CommonBundle\Service\TradingPlatform\Adapter\Simulator;
use CommonBundle\Service\TradingPlatform\Provider\PlatformLinkInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * @method SimulatorAccountRepository getRepository()
 */
class SimulatorAccountManager extends AbstractManager
{
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        parent::__construct($documentManager, SimulatorAccount::class);
    }

    /**
     * Creates simulator accounts for the APIPass and returns PlatformLink
     * @param \CommonBundle\Document\Types\CurrencyPair $currencyPair
     * @return \CommonBundle\Document\PlatformLink
     */
    public function attachAccounts(CurrencyPair $currencyPair): PlatformLink
    {
        $quoteAccount = new SimulatorAccount($currencyPair->quote);
        $quoteAccount->setAmount(BigDecimal::of(BotSettings::AMOUNT_BORDER_DEFAULT));
        $this->persist($quoteAccount);
        $baseAccount = new SimulatorAccount($currencyPair->base);
        $this->persist($baseAccount);
        
        $this->flush();

        return new PlatformLink(
            Simulator::class,
            (string)$currencyPair,
            $baseAccount->getId(),
            $quoteAccount->getId()
        );
    }
}